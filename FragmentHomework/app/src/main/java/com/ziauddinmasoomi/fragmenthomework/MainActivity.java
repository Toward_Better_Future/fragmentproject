package com.ziauddinmasoomi.fragmenthomework;

import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button next,previous;
    private FragmentManager manager;
    private FragmentTransaction transaction;
    private static int counter = 0;
    private Fragment[] layouts;
    private boolean condition = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        initialization();

        layouts = new Fragment[]{new First(),new Second(),new Third(),new Fourth(),new Fifth(),
                new Seventh(),new Eighth(),new Nineth(),new Tenth()};


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                condition = true;
                manager = getFragmentManager();
                transaction = manager.beginTransaction();
                if (counter < 0 ){

                    counter = 1;
                    transaction.replace(R.id.imageLayout,layouts[counter++]);
                    transaction.commit();
                }
                else if (counter < layouts.length){
                   transaction.replace(R.id.imageLayout,layouts[counter++]);
                   transaction.commit();
               }
                else {
                   counter = 0;
               }
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                manager = getFragmentManager();
                transaction = manager.beginTransaction();

                if (condition){
                    counter -= 2;
                    condition = false;
                }

                if ( counter >= 0){
                    transaction.replace(R.id.imageLayout,layouts[counter--]);
                    transaction.commit();
                }
               if(counter < 0) {
                    Toast.makeText(getApplicationContext(),"Start point of the list",Toast.LENGTH_SHORT);
                }
            }
        });

    }

    private void initialization(){
        next = (Button) findViewById(R.id.nextBtn);
        previous = (Button) findViewById(R.id.previousBtn);
    }
}
